# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

![Image](Info.png)

Paragraph with **strong**, _emphasis_, ~~strikethrough~~, `code`, a footnote[^Footnote content.], and a [link](https://ia.net/writer).

- List item with [table reference][table-identifier]
- [ ] Task list item

---

1. Ordered list item
1. [x] Ordered task list item

> Block quote

```swift
print("Hello World!")
```

| | Grouping ||  
| First Column | Second Column | Third Column |  
| ------------ | :-----------: | -----------: |  
| Cell | _Long Cell_ ||  
| Cell | **Cell** | Cell |
[Table Caption][table-identifier]

Citation from a book.[p. 42][#book]

[#book]: John Doe. _A Totally Fake Book_. Vanity Press, 2006.
